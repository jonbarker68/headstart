"""Simple pygame painting robot."""
import pygame
import os
import sys

class Robot(object):
    """Class to represent the painting robot."""

    N = (10, 10)    # Number of board rows and columns on board
    NCOLOURS = 4    # Number of colours allowed
    SQSIZE = 60     # Size of single square in pixels
    NSTEPS = 10     # Number of steps for animation
    # defines the 4 directions in terms of changes to x and y
    OFFSET = ((0, -1), (1, 0), (0, 1), (-1, 0))

    def __init__(self):
        """Robot constructor."""
        self.dir = 1          # direction in which robot is facing
        self.pos = [0, 0]     # xposition of robot
        self.board = [[0 for x in range(Robot.N[0])]
                      for y in range(Robot.N[1])]
        self.delay = 10
        os.environ['SDL_VIDEO_CENTERED'] = '1'  # Center the screen.
        pygame.init()
        self.display = pygame.display.set_mode((Robot.SQSIZE * Robot.N[0],
                                                Robot.SQSIZE * Robot.N[1]))
        self._draw_display()

    def set_speed(self, speed):
        """Set the animation speed (0 = slowest; 10 = fastest)."""
        self.delay = 10 - speed
        if self.delay < 0:
            self.delay = 0

    def turn_left(self):
        """Turn robot 90 degrees to the left."""
        self.dir = (self.dir - 1) % 4
        self._draw_display()

    def turn_right(self):
        """Turn robot 90 degrees to the right."""
        self.dir = (self.dir + 1) % 4
        self._draw_display()

    def move_forward(self):
        """Move the robot one square forward if possible."""
        status = 0
        if self._try_move(Robot.OFFSET[self.dir]) != self.pos:
            self._animate_move(Robot.OFFSET[self.dir])
            status = 1
        return status

    def look_down(self):
        """Return the colour of the robot's square."""
        return self.board[self.pos[0]][self.pos[1]]

    def look_ahead(self):
        """Return colour of square in front of robot or -1 if facing edge."""
        aheadPos = self._try_move(Robot.OFFSET[self.dir])
        if aheadPos == self.pos:
            code = -1
        else:
            code = self.board[aheadPos[0]][aheadPos[1]]
        return code

    def paint(self, colour):
        """Set the colour of the robot's square."""
        self.board[self.pos[0]][self.pos[1]] = colour % Robot.NCOLOURS
        self._draw_display()

    def load_board(self, exercise):
        """Load preset boards for lab exercises."""
        status = 1
        if exercise == 1:
            self.board = [
                [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
            ]
        elif exercise == 2:
            self.board = [
                [0, 1, 0, 1, 0, 1, 0, 1, 0, 1],
                [1, 0, 1, 0, 1, 0, 1, 0, 1, 0],
                [0, 1, 0, 1, 0, 1, 0, 1, 0, 1],
                [1, 0, 1, 0, 1, 0, 1, 0, 1, 0],
                [0, 1, 0, 1, 0, 1, 0, 1, 0, 1],
                [1, 0, 1, 0, 1, 0, 1, 0, 1, 0],
                [0, 1, 0, 1, 0, 1, 0, 1, 0, 1],
                [1, 0, 1, 0, 1, 0, 1, 0, 1, 0],
                [0, 1, 0, 1, 0, 1, 0, 1, 0, 1],
                [1, 0, 1, 0, 1, 0, 1, 0, 1, 0]
            ]
        elif exercise == 3:
            self.board = [
                [1, 1, 1, 1, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 1, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 1, 0, 1, 1, 1, 1, 0],
                [0, 1, 1, 1, 0, 1, 0, 0, 1, 0],
                [0, 1, 0, 0, 0, 1, 0, 0, 3, 0],
                [0, 1, 0, 0, 0, 1, 0, 0, 0, 0],
                [0, 1, 0, 0, 0, 1, 1, 1, 0, 0],
                [0, 1, 0, 0, 0, 0, 0, 1, 0, 0],
                [0, 1, 1, 1, 1, 1, 1, 1, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
            ]
        elif exercise == 4:
            self.board = [
                [1, 1, 1, 1, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 1, 1, 1, 1, 1, 1, 0],
                [0, 0, 0, 1, 0, 0, 0, 0, 0, 0],
                [0, 1, 1, 1, 0, 1, 1, 1, 1, 0],
                [0, 1, 0, 0, 0, 1, 0, 0, 3, 0],
                [0, 1, 0, 0, 0, 1, 0, 0, 0, 0],
                [0, 1, 0, 1, 1, 1, 1, 1, 0, 0],
                [0, 1, 0, 0, 0, 0, 0, 1, 0, 0],
                [0, 1, 1, 1, 1, 1, 1, 1, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
            ]
        elif exercise == 5:
            self.board = [
                [1, 1, 1, 1, 0, 1, 0, 0, 1, 1],
                [0, 0, 0, 1, 1, 1, 1, 1, 1, 0],
                [0, 0, 0, 1, 0, 0, 1, 0, 0, 0],
                [0, 1, 1, 1, 0, 1, 1, 1, 1, 0],
                [0, 1, 0, 0, 0, 1, 0, 0, 3, 0],
                [0, 1, 0, 0, 0, 1, 0, 0, 0, 0],
                [0, 1, 0, 1, 1, 1, 1, 1, 0, 0],
                [0, 1, 0, 0, 0, 0, 0, 1, 0, 0],
                [0, 1, 1, 1, 1, 1, 1, 1, 0, 0],
                [0, 0, 1, 0, 1, 0, 0, 0, 0, 0]
            ]
        elif exercise == 6:
            self.board = [
                [1, 1, 1, 1, 0, 1, 0, 0, 1, 1],
                [0, 0, 0, 1, 1, 1, 1, 1, 1, 0],
                [0, 0, 0, 1, 0, 0, 1, 0, 0, 0],
                [0, 1, 1, 1, 0, 1, 1, 1, 1, 0],
                [0, 1, 0, 0, 0, 1, 0, 0, 1, 0],
                [0, 1, 0, 0, 0, 1, 0, 0, 0, 0],
                [0, 1, 0, 3, 1, 1, 1, 1, 0, 0],
                [0, 1, 0, 0, 0, 0, 0, 1, 0, 0],
                [0, 1, 1, 1, 1, 1, 1, 1, 0, 0],
                [0, 0, 1, 0, 1, 0, 0, 0, 0, 0]
            ]
        else:
            print("load_board: invalid exercise number")
            status = 0

        if status:
            # transpose board because I entered the patterns wrong way around!
            for i in range(10):
                for j in range(i):
                    x = self.board[j][i]
                    self.board[j][i] = self.board[i][j]
                    self.board[i][j] = x
            self._draw_display()

        return status

    def wait(self):
        """Wait for  key to be pressed."""
        print("Press space to continue...")
        pygame.time.delay(200)
        pressed = 0
        while not pressed:
            pygame.event.get()
            keys = pygame.key.get_pressed()
            pressed = keys[pygame.K_SPACE]
            pygame.event.pump()

    def quit(self):
        """Close the display before quitting."""
        pygame.display.quit()

    # private methods

    def _try_move(self, delta):
        """Add delta to current position but constrain to bounds."""
        x = self.pos[0] + delta[0]
        y = self.pos[1] + delta[1]
        x = max(0, min(Robot.N[0] - 1, x))
        y = max(0, min(Robot.N[1] - 1, y))
        return [x, y]

    def _animate_move(self, delta):
        """Move the robot by xdelta and ydelta and animate the motion."""
        xdelta = delta[0]
        ydelta = delta[1]
        # Optimise the drawing range so that only effected squares are redrawn
        if xdelta != 0:
            x_range = range(max(self.pos[0] - (xdelta < 0), 0),
                            min(self.pos[0] + 2, Robot.N[0]))
        else:
            x_range = [self.pos[0]]

        if ydelta != 0:
            y_range = range(max(self.pos[1] - (ydelta < 0), 0),
                            min(self.pos[1] + 2, Robot.N[1]))
        else:
            y_range = [self.pos[1]]

        # Move the robot from one square to the next using NSTEPS
        stepSize = 1.0 / Robot.NSTEPS
        if self.delay > 0:
            for i in range(1, Robot.NSTEPS + 1):
                self._draw_display(x_range, y_range,
                                   i * stepSize * xdelta,
                                   i * stepSize * ydelta)
        self.pos[1] = self.pos[1] + ydelta
        self.pos[0] = self.pos[0] + xdelta

    def _draw_display(self, x_range=0, y_range=0, xoffset=0, yoffset=0):
        """Draw the complete display. Called whenever the state has changed."""
        # Define the colors we will use in RGB format
        colours = ((0, 0, 0),
                   (255, 255, 255),
                   (0, 0, 255),
                   (255, 0, 0))

        if x_range == 0:
            x_range = range(Robot.N[0])
        if y_range == 0:
            y_range = range(Robot.N[1])

        for x in x_range:
            for y in y_range:
                colour = self.board[x][y]
                pygame.draw.rect(self.display, colours[colour % 4],
                                 [x * Robot.SQSIZE, y * Robot.SQSIZE,
                                 Robot.SQSIZE, Robot.SQSIZE], 0)
        pygame.draw.rect(self.display, (0, 255, 0),
                         [(self.pos[0] + xoffset) * Robot.SQSIZE + 4,
                         (self.pos[1] + yoffset) * Robot.SQSIZE + 4,
                         Robot.SQSIZE - 8, Robot.SQSIZE - 8], 4)

        offset = Robot.OFFSET[self.dir]
        centre = ((self.pos[0] + (xoffset + 0.5)) * Robot.SQSIZE,
                  (self.pos[1] + (yoffset + 0.5)) * Robot.SQSIZE)
        pygame.draw.line(self.display, (128, 128, 128), centre,
                         (centre[0] + offset[0] * Robot.SQSIZE / 2,
                         centre[1] + offset[1] * Robot.SQSIZE / 2), 6)
        pygame.display.flip()
        pygame.time.delay(self.delay)
