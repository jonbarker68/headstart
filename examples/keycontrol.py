"""Demo of keyboard controlled robot."""
from robot import Robot
import pygame as pg
import sys

exercise = 1

if len(sys.argv) == 2:
    exercise = sys.argv[1]

arobot = Robot()
arobot.set_speed(1)

arobot.load_board(exercise)

done = 0

while True:
    for j in range(10):
        for i in range(10):
            if (arobot.look_down() == 1):
                arobot.paint(0)
            elif (arobot.look_down() == 0):
                arobot.paint(1)
            arobot.move_forward()
        arobot.turn_right()
        arobot.turn_right()
        for i in range(9):
            arobot.move_forward()
        arobot.turn_left()
        arobot.move_forward()
        arobot.turn_left()
    arobot.turn_left()
    for j in range(9):
        arobot.move_forward()
    arobot.turn_right()

while not done:
    moved = 0
    pg.time.delay(10)
    pg.event.get()
    keys = pg.key.get_pressed()
    if keys[pg.K_RIGHT]:
        moved = arobot.turn_right()
        pg.time.delay(200)
    elif keys[pg.K_LEFT]:
        moved = arobot.turn_left()
        pg.time.delay(200)
    elif keys[pg.K_UP]:
        moved = arobot.move_forward()
        pg.time.delay(200)
    elif keys[pg.K_p]:
        c = arobot.look_down()
        arobot.paint((c + 1) % 4)
        pg.time.delay(200)
    elif keys[pg.K_q]:
        done = 1
