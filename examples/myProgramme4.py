"""Robot module demo."""
from robot import Robot

robby = Robot()

robby.load_board(5)  # Works for board 5 ... :)

# robby.load_board(6)  # ... but will fail for board 6. :(

robby.paint(2)
i = 0

while 1:
    colour = robby.look_ahead()
    if colour == 3:
        robby.move_forward()
        break
    if colour <= 0:
        robby.turn_right()
    else:
        robby.move_forward()
        robby.paint(2)
    if i % 2 == 1:
        robby.turn_left()
    i += 1

robby.wait()
robby.quit()
