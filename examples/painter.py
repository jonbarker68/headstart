"""Simple demo of the robot module."""
from robot import Robot

arobot = Robot()

colour = 1
squaresize = 9

for k in range(5):
    for j in range(4):
        for i in range(squaresize):
            arobot.move_forward()
            arobot.paint(colour)
            colour = (colour + 1) % 3
        arobot.turn_right()
    arobot.move_forward()
    arobot.turn_right()
    arobot.move_forward()
    arobot.turn_left()
    squaresize = squaresize - 2

arobot.wait()
