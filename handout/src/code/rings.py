"""Example of drawing a complex pattern using simple functions."""
from robot import Robot


def draw_line(length, colour):
    """Paint a straight line."""
    for j in range(length):
        robby.paint(colour)
        robby.move_forward()


def draw_square(size, colour):
    """Paint a square."""
    for i in range(4):
        draw_line(size, colour)
        robby.turnRight()

robby = Robot()

for i in range(3):
    draw_square(5, i + 1)
    robby.move_forward()
    robby.move_forward()
    robby.turn_right()
    robby.move_forward()
    robby.move_forward()
    robby.turn_left()

robby.wait()
robby.quit()
