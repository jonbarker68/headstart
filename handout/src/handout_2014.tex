\documentclass[11pt]{article}

\usepackage[utf8]{inputenc}
\usepackage{graphicx}
\usepackage{a4wide}
\usepackage{titling}
\usepackage{lipsum}

\usepackage{fancyhdr}
\pagestyle{fancy}

\setlength{\droptitle}{-10em}   % This is your set screw

% Default fixed font does not support bold face
\DeclareFixedFont{\ttb}{T1}{txtt}{bx}{n}{12} % for bold
\DeclareFixedFont{\ttm}{T1}{txtt}{m}{n}{12}  % for normal

% Custom colors
\usepackage{color}
\definecolor{deepblue}{rgb}{0,0,0.5}
\definecolor{deepred}{rgb}{0.6,0,0}
\definecolor{deepgreen}{rgb}{0,0.5,0}
\definecolor{midgray}{rgb}{0.5,0.5,0.5}

\usepackage{listings}

% Python style for highlighting
\newcommand\pythonstyle{\lstset{
language=Python,
basicstyle=\ttm,
otherkeywords={self},             % Add keywords here
keywordstyle=\ttb\color{deepblue},
emph={MyClass,__init__},          % Custom highlighting
emphstyle=\ttb\color{deepred},    % Custom highlighting style
stringstyle=\color{deepgreen},
frame=tblr,                         % Any extra options here
showstringspaces=false            %
}}


\newcommand\pythonstylef{\lstset{
language=Python,
basicstyle=\ttm,
otherkeywords={self},             % Add keywords here
keywordstyle=\ttb\color{deepblue},
emph={MyClass,__init__},          % Custom highlighting
emphstyle=\ttb\color{deepred},    % Custom highlighting style
stringstyle=\color{deepgreen},
frame=tblr,                         % Any extra options here
float=h,
showstringspaces=false            %
}}


% Python environment
\lstnewenvironment{python}[1][]
{
\pythonstyle
\smallskip
\lstset{#1}
\smallskip
}
{}

% Python environment
\lstnewenvironment{pythonf}[1][]
{
\pythonstylef
\smallskip
\lstset{#1}
\smallskip
}
{}

% Python for external files
\newcommand\pythonexternal[1]{{
\pythonstyle
\lstinputlisting{#1}}}

% Python for inline
\newcommand\pythoninline[1]{{\pythonstyle\lstinline!#1!}}

\usepackage{listings}

\date{July 2014}

\chead{An Introduction to Programming using Python}
\cfoot{\color{midgray} Headstart Computer Science Summer School, University of Sheffield, 2014}
\rhead{\thepage}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\title{\textbf{An Introduction to Programming using Python} \\ Headstart Computer Science Summer School\\ University of Sheffield}
\author{Jon Barker}

\begin{document}
\lstset{language=Python}

\maketitle



\section{Introduction}
\label{sec:intro}

The purpose of this session is to introduce you to a set of basic programming concepts that are present in all programming languages. In particular we will be looking at \textbf{loops}, \textbf{functions}, \textbf{variables} and \textbf{decision making}. In isolation these concepts are all extremely simple -- they could be explained to a 5 year old -- but by putting them together we can produce computer programs of great sophistication and complexity.

 In order to illustrate how complexity can arise from simplicity the lab class is going to use the example of \textbf{a robot painter that can move about on a board and change the colour of the board squares}. We will start out by making the robot draw simple lines and shapes but by the end of the class we will see how we can simulate the seemingly intelligent behaviour of a rat searching for food in a maze.

The session will be using \textbf{a programming language called Python}. You may or may not have done some Python programming before. It is possible that you haven't done any programming before. If you haven't then don't panic! We will start out by taking small steps. The session is less about learning Python and more about learning to think \textit{algorithmically}  so I have tried to keep the amount of Python syntax to a minimum. However, \textbf{if you get stuck don't be shy about asking for help -- just raise a hand}. Also, feel free to help each other and to discuss the problems amongst yourselves. Work in pairs at one machine if you like. Most importantly, experiment and have fun!

\section{First steps}
\label{sec:first}

\begin{enumerate}
% THIS STEP HAS BEEN REMOVED AS IT SHOULD ALREADY HAVE BEEN DONE
%
%\item \textbf{Get a copy of the example programs.} Double-click the Computer icon on the Desktop.  Double click on the openday drive (see figure below).  A file viewer will open. Double click on the folder \verb|1_python|. Double click on the file \verb|DOUBLE_CLICK_ME.bat|. This will copy some files into your local \texttt{Documents} folder.
%
%\medskip
%
%
%\centerline{\includegraphics[width=0.5\linewidth]{graphics/CaptureDrive.png}}
%
\item \textbf{Start Canopy}. (If Canopy is already running then skip to step 2). From the Windows Start menu type \texttt{canopy} in the search box and press return. If starting for the first time Canopy will take a few minutes to load. Please be patient or read ahead while you wait.

\medskip

\centerline{\includegraphics[width=0.5\linewidth]{graphics/CaptureStart.png}}

\item \textbf{Start the Canopy Editor.} Once Canopy has started click the `Edit Program' icon that appears on the left of the Canopy startup screen.

\medskip

\centerline{\includegraphics[width=0.2\linewidth]{graphics/CaptureEditIcon.png}}


\begin{figure}[ht]
\centerline{\includegraphics[width=0.75\linewidth]{graphics/CaptureCanopy.png}}
\caption{The Canopy IDE interface}
\label{fig:canopy}
\end{figure}

\item \textbf{Point Canopy to the correct folder.} At the Canopy shell prompt (arrow 1 in Figure \ref{fig:canopy}) type, \verb|cd Documents\1_python|
\item \textbf{Write your first program!} In the text editing window (arrow 2 in Figure \ref{fig:canopy}) first click the `Create a new file' button and then carefully type in the following program,

\begin{python}
from robot import Robot
robby = Robot()
robby.wait()
robby.quit()
\end{python}
%
Don't worry about the details of the code above but roughly speaking it works as follows. The first line tells Python that you are going to be using code that describes a Robot that is stored in a file \texttt{robot.py} that we have written for you. The second line tells Python to make  Robot called robby that will be displayed in a window. The third line makes the robot wait until space is pressed. The final line closes the program and deletes the display.
%
\item \textbf{Run the program.} To run the program simply click on the green triangle in the Canopy toolbar (arrow 3 in Figure \ref{fig:canopy}).
\end{enumerate}

If all has gone well a window should appear showing a black-backgrounded painting area with your robot positioned in the top-left hand corner. The Canopy Python shell should display ``Press space to continue''.  If  you press space the robot program will stop running, i.e.~the window will disappear and the Canopy Python shell will display a fresh prompt (e.g.~``\verb|In [2]:|'') to show that it is waiting for more input.

\begin{figure}[ht]
\centerline{\includegraphics[height=0.25\linewidth]{graphics/HSStart.png} \quad \quad \includegraphics[height=0.25\linewidth]{graphics/HSLshape.png}}
\caption{\textbf{Left}: Robot in initial position. \textbf{Right}: Position after code in Section \ref{sec:controlling}.}
\label{fig:initial}
\end{figure}

\section{Controlling the Robot}
\label{sec:controlling}

The Robot has a number of actions that it can perform.  These can be performed by issuing the following instructions,

\begin{itemize}
\item \pythoninline{robby.turn\_left()}  --- turn left by 90 degrees.
\item \pythoninline{robby.turn\_right()} --- turn right by 90 degrees.
\item \pythoninline{robby.move\_forward()} --- move forward by one square.
\item \pythoninline{robby.paint(colour)} – paint the square a given colour. Note, \texttt{colour} can have the value 0, 1, 2 or 3 for black, white, blue or red respectively, e.g.~\texttt{robby.paint(2)} will paint the square blue.
\end{itemize}

Let’s use these instructions to make the robot draw a simple pattern.

\begin{enumerate}
\item Edit the code that you wrote in the previous section so that it now reads as follows,

\medskip


\begin{python}
from robot import Robot
robby = Robot()
robby.paint(1)
robby.move_forward()
robby.paint(1)
robby.turn_right()
robby.move_forward()
robby.paint(1)
robby.wait()
robby.quit()
\end{python}

\item Run the program again by clicking the green arrow.
\end{enumerate}

If things have gone well then you should see the robot paint a white `L' shape and end up in the position shown in the right panel of Figure \ref{fig:initial}. Check that you understand what each line of the program is doing.

\subsection*{End of Section Challenge:}

\begin{itemize}
\item Edit the program so that Robby draws a horizontal red line of length 5.
\item Edit the program so that Robby draws a vertical blue line of length 4.
\end{itemize}

\section{Introducing loops}
\label{sec:loops}

In the last section you wrote a program to draw a long line. You probably did this by simply repeating the instruction \texttt{move\_forward()} and \texttt{paint()}. This takes a lot of typing and is rather inflexible – later we will want to be able to draw lines of different lengths. A more powerful way to repeat instructions is to use a programming construct called a \textit{loop}.

The following code uses a loop to print \texttt{Hello} followed by \texttt{Goodbye} ten times in a row,

\begin{python}
for i in range(10):
    print('Hello')
    print('Goodbye')
\end{python}

The line starting `\texttt{for}’ tells Python that this is a loop and indicates how many times the loop should be repeated (the number of \textit{iterations}). The instructions that are indented below the line starting `\texttt{for}’ are the loop’s \textit{body}. These are the instructions that will be repeated. \textbf{Warning: the indentation is important} -- Python uses the indentation to see where the body starts and ends.

\begin{itemize}
\item Rewrite your program for drawing a horizontal or vertical line but this time use a loop. Change the number of iterations to produce lines of different lengths. What happens if you try to draw a line that is longer than 10?
\end{itemize}

%\section{Nested loops – loops inside loops}
\label{sec:nested}

The body of a loop can be any sequence of Python instructions. So the following bit of code is perfectly valid,

\begin{python}
for i in range (3):
    print('outer loop')
    for j in range(5):
        print('inner loop')
\end{python}

Study the code carefully and try and work out what output it will produce.

Here, a new `inner' loop is appearing inside the body of an `outer’ loop: a loop inside a loop. This structure is called a \textit{nested loop}. Loops can be nested to any depth, i.e.~we can have a loop that’s inside a loop that’s inside a loop, etc.

Note that the outer loop starts `\texttt{for i …}’  and the inner loop starts `\texttt{for j …}’ . The \texttt{i} and \texttt{j} are called the \textit{loop variables}. \texttt{i} and \texttt{j} are arbitrary names -- I could have called them anything else. Variables are used for storing values that the program needs to remember. The loop variables will store how many times we have been around the loop. We will discuss variables in more detail in Section \ref{sec:variables}.

\subsection*{Challenge}

\begin{itemize}
\item Using a nested loop, extend your line-drawing  program so that it draws a blue box around the edge of the painting area (as in Figure \ref{fig:blueframe}).
\begin{itemize}
\item \textbf{Tip:}  The edges of the box are drawn by an inner loop that sits inside an outer loop that repeats 4 times. Remember to turn the robot after drawing each edge.
\end{itemize}
\end{itemize}

\begin{figure}[ht]
\centerline{\includegraphics[height=0.2\linewidth]{graphics/HSblueframe.png}}
\caption{Board and robot position after drawing a blue border.}
\label{fig:blueframe}
\end{figure}

\section{Functions }
\label{sec:func}

In the previous section we ended by writing some Python code that drew a blue border. This is a useful thing that might need to be done several times at different places in a more complex program. We don’t want to have to retype the code for doing something every time we want to do it. We can solve this problem by wrapping up these bits of code in a structure called a \textit{function} to which we can assign a \textit{function name}. Once a function has been defined it can be performed by calling its name.

To define a new function in Python we write,
%
\begin{python}
    def function_name():
\end{python}
%
where `\texttt{function\_name}’ can be any name that we chose. We then write the \textit{body} of the function (i.e.~the instructions that it performs) on the following lines. Each line in the body must be indented. To call the function we then just need to write \texttt{function\_name()} at some point in the program after the function has been defined. This is made clear by the example program shown in Figure \ref{code:1} (next page).

\begin{figure}[ht]
\begin{python}
from robot import Robot

# define a function called drawLine
def draw_line():
    for j in range(10):
        robby.paint(2)
        robby.move_forward()

# body of the main program
robby = Robot()

# draw lines by calling my drawLine function
draw_line()
robby.turn_right()
draw_line()

robby.wait()
robby.quit()
\end{python}
\caption{A Python program illustrating the definition and use of a function.}
\label{code:1}
\end{figure}

In this example the lines starting with `\verb|#|' are \textit{comments}. Comments are ignored by Python. So why are they there?  Well-chosen comments can make the code more understandable to other readers (and ourselves!)

\subsection*{Challenge}

\begin{itemize}
\item Start by typing in the code above that defines and uses the function \texttt{draw\_line()}. Check that it works.
\item Now, add a new function called \texttt{draw\_square()} that uses the \texttt{draw\_line()} function to paint a blue border. (The new function should be added after the definition of \texttt{draw\_line()} and before the main program body.)
\end{itemize}



\section{Introducing variables}
\label{sec:variables}

In the previous section we wrote a function that drew a blue square of size 10. This might be useful but what if later we want to draw a \textit{red} square or a \textit{smaller} square? It would be much more useful if our function could draw a square of arbitrary size and colour. We can achieve this by using the idea of a variable.

A variable is like a labeled bucket that we can use to store a value. We can put a value in the bucket (\textit{assignment}) or we can look in the bucket and see what it holds. For example, we can make a bucket called \textit{length} and put the number 10 inside it,

\begin{python}
length = 10
\end{python}
%
Then we can use the value in the bucket by referring to the bucket’s name,

\begin{python}
print(length + 5)
\end{python}
%
This will print `15'. (Try typing these lines directly into the Python shell.)

Or, let's say that we want to change the value stored in the bucket. Say we want to increase it by 2. This can be done by typing,

\pagebreak

\begin{python}
length = length + 2
print(length)
\end{python}
%
Again, you can test this by typing the lines directly into the Python shell. Now consider the following bit of code,

\begin{python}
length = 10
colour = 3
for j in range(length):
    robby.paint(colour)
    robby.move_forward()
\end{python}
%
This will draw a line of length 10 and colour 3.

We can now use the idea of a variable to define a \textit{parameterized function}. Look carefully at the code below,

\begin{python}
def drawLine(length, colour):
    for j in range(length):
        robby.paint(colour)
        robby.move_forward()
\end{python}

This defines a function called \texttt{draw\_line} that has two \textit{parameters}, i.e.~it has two inputs. To use this function we pass it values that it will use to store in the variable \texttt{length} and \texttt{colour}. For example,

\begin{python}
draw_line(10, 3)
\end{python}
%
will draw a line of length 10 and colour 3, but,

\begin{python}
draw_line(5, 2)
\end{python}
%
will draw a line of length 5 and colour 2.

\subsection*{Challenge:}

\begin{itemize}
\item Edit the \texttt{draw\_line} and \texttt{draw\_square} functions that you wrote in the previous section so that they now take a length and colour parameter.  Use your new function to write a program that produces the pattern shown in Figure \ref{fig:blueredframe}.
\end{itemize}


\begin{figure}[ht]
\centerline{\includegraphics[height=0.2\linewidth]{graphics/HSblueredframe.png}}
\caption{A red frame inside a blue frame (\texttt{borders}).}
\label{fig:blueredframe}
\end{figure}


\section{Using the loop variable}
\label{sec:loopvar}

Remember that the loop statement includes a variable name, the \textit{loop variable}. For example,

\begin{python}
for i in range(10):
\end{python}
%
The loop variable is used by Python to keep track of how many times we have been around the loop. To see how it behaves try typing the following bit of code directly into the Python shell (i.e.~after the ``\verb|In []:|'' prompt).

\begin{python}
for i in range(10):
    print(i)
\end{python}

Look carefully at what is printed. Can you see what is happening? Each time around the loop that value of i is increased, starting at 0 and ending at 9. This is very useful. For example, look at the code below -- try guessing what it is going to do and then enter it into the shell to see if you are right,

\begin{python}
for i in range(10):
    print(i , ' squared is ', i * i)
\end{python}

We can now combine this idea with the parameterized functions from the previous section. For example, try adding the following lines to the end of your program in the editor,

\begin{python}
for i in range(10):
    draw_sqauare(i, i)
\end{python}

If you run the program the robot should generate the pattern shown in the left panel of Figure \ref{fig:squares}.


\begin{figure}[ht]
\centerline{\includegraphics[height=0.2\linewidth]{graphics/HSChevrons.png} \hfil \includegraphics[height=0.2\linewidth]{graphics/HSBullseye.png}}
\caption{Patterns created by drawing squares of decreasing size (\texttt{chevrons} and \texttt{bullseye}).}
\label{fig:squares}
\end{figure}

\subsection*{Challenge}

\begin{itemize}
\item Try and modify the code you have just written so that it produce the pattern shown on the right hand side of Figure \ref{fig:squares}.
\item The patterns shown in Figure \ref{fig:patterns} can all be generated by programs that use simple (non-nested) loops that call \texttt{draw\_line()}, \texttt{draw\_square()} and the robot move functions.  See if you can reproduce them.
\end{itemize}

\begin{figure}[ht]
\centerline{\includegraphics[height=0.2\linewidth]{graphics/HSStairs.png} \hfil
\includegraphics[height=0.2\linewidth]{graphics/HSStripes.png} \hfil
\includegraphics[height=0.2\linewidth]{graphics/HSRings.png}}
\caption{Patterns created by simple programs using loops (\texttt{stairs}, \texttt{stripes} and \texttt{rings}).}
\label{fig:patterns}
\end{figure}


\section{Making decisions}
\label{sec:if}

We have seen how by writing simple programs we can instruct the robot to produce surprisingly complex patterns. However, although the robot is moving in complicated patterns it is not responding to its environment. It always behaves the same way. We will now do something a bit more interesting: we will write a program in which the robot interacts with its environment -- its behaviour will depend on how the board is initially painted.

In order to allow the robot to interact with the board pattern we will introduce two more robot functions,

\begin{itemize}
\item \pythoninline{robby.look_down()} -- returns the colour of the square where the robot is positioned.
\item \pythoninline{robby.look_ahead()}  -- returns the colour of the square in front of the robot, or -1 if the robot is facing the edge of the area.
\end{itemize}

To see how these work try the following,

\begin{python}
robby.paint(3);
colour_below = robby.look_down()
colour_infront = robby.look_ahead()
print colour_below, colour_infront
\end{python}

We've also provided you with a function that will initialize the painting area with an interesting pattern,
%
\begin{itemize}
\item \pythoninline{robby.load\_board(pattern\_number)} -- where \texttt{pattern\_number} can be 1 to 6.
\end{itemize}
%
We will now write a program in which the robot looks at the colour of the board and does something that depends on the colour. To do this we will have to introduce a new Python programming concept: something called an \textit{if-statement}. If-statements look like this,

\begin{python}
if condition:
    do-something
\end{python}
%
Or this,

\begin{python}
if condition:
    do-something
else:
    do-something-different
\end{python}

The bit called `\texttt{condition}’ is a \textit{logical test} that can be evaluated as either true or false.  There are many different tests we can use. Some examples include,
%
\begin{itemize}
\item \pythoninline{if (x == 0):}   -- true if the value of x equals 0
\item \pythoninline{if (x \!= 0):}   -- true if the value of x is not equal to 0
\item \pythoninline{if (x < 2):}    -- true if the value of the variable x is less than 2
\item \pythoninline{if (x >= 1):}    -- true if the value of x is greater than or equal to 1
\end{itemize}

So look at the code below and try and work out what it does. (Note, the code would usually have comments to make it easier to understand, but I have deliberately left them out this time!)

\begin{python}
from robot import Robot
robby = Robot()
robby.load_board(2)  # load checkerboard pattern

for i in range(10):
    for i in range(10):
        colour = robby.look_down()
        if colour == 0:
            robby.paint(1)
        else:
            robby.paint(0)
        robby.moveForward()
    robby.turn_right()
    robby.turn_right()
    for i in range(10):
        robby.move_forward()
    robby.turn_left()
    robby.move_forward()
    robby.turn_left()

robby.wait()
robby.quit()
\end{python}

To save you some typing the program has been provided for you. To load it you can use the Canopy File `Load' menu item. Select the program \texttt{checkerboard.py} that should be in the \texttt{1\_python} folder. Load it, run it and check that it behaves as you expected.


\section{Following a path}
\label{sec:path}

In this final section we are going to see how, with a surprisingly simple program, we can generate \textit{seemingly} intelligent behaviour. We will program Robby to follow a path until she finds some treasure.

In this task the paths are going to be defined by white squares on a black background. At some point on the path there will be a red square (the treasure). Robby must follow the path without moving into any of the black areas (the walls). Robby should stop when she is facing the red square. To help her remember where she has been she will paint the path blue as she moves.


\begin{figure}[ht]
\centerline{\includegraphics[height=0.24\linewidth]{graphics/HSMaze1.png} \hfil
\includegraphics[height=0.24\linewidth]{graphics/HSMaze2.png} \hfil
\includegraphics[height=0.24\linewidth]{graphics/HSMaze3.png} \hfil
\includegraphics[height=0.24\linewidth]{graphics/HSMaze4.png}}
\caption{Initial positions after loading boards 3, 4, 5 and 6. The challenge is to write a program that navigates the robot to the red square without moving onto the black areas.}
\label{fig:mazes}
\end{figure}

The first path we are going to use is a simple path with no branches (leftmost image in Figure \ref{fig:mazes}).

\begin{python}
robby.load_board(3)  # load path pattern
\end{python}

Below is a simple algorithm that will allow Robby to follow this path and reach the treasure,

\begin{python}
for lots of time
    Look Forward
    if the square is red
        Quit
    else if the square is not white
        Turn Right
    else
        Paint the square blue
  	Move Forward
\end{python}

Note that this algorithm is not written in Python. It is a semi-formal `sketch' of a Python program written in something that is between English and formal Python code. This is called \textit{pseudocode}. Programmers often write and discuss algorithms using pseudocode before converting the pseudocode into real code.

\pagebreak

\subsection*{Challenge}

\begin{itemize}
\item Try and turn the pseudocode above into a real Python program.
\end{itemize}


\textbf{Tip:} \textit{If-statements} can be nested. For example,



\begin{python}
if x < 6:
    print('x is less than 6')
else:
    if x < 11:
        print('x is in the range 6 to 10')
    else:
        print('x is greater than 10')
\end{python}
%
The nested if statements above can be written in a simpler form using an \textit{if-elif-else} structure,
%
\begin{python}
if x < 6:
    print('x is less than 6')
elif x < 11:
    print('x is in the range 6 to 10')
else:
    print('x is greater than 10')
\end{python}
%
\textbf{If you get stuck you can load a correct solution (\texttt{mazeSolution1.py}) from the solutions folder.}

\subsection*{Super challenge}

Once you have solved the first maze, change the code so that the program starts by loading board 4 instead of 3. Try running it again. What goes wrong. How might you improve the algorithm so that it works for this new maze?

\textit{hint: What path would the robot would take if it were following a wall?}

If you implement a wall-following strategy the robot will find the solution to board 4. The same approach should work equally well for the more complicated path in board 5. But what happens when you try board 6.

Think about how you might program the robot to systematically explore every passageway of an arbitrary maze and always find treasure.



\section{Next steps}
This session has provided the briefest introduction to programming and Python. We have focused on a small subset of concepts that lie at the heart of Python -- and other programming language. We have seen that from these simple roots we can grow sophisticated behaviours. However, there is \textit{a lot} of important stuff that we simply haven't had time to cover.  If you have enjoyed this session you may wish to take your Python programming further. Fortunately Python is free to install on your computer and there are plenty of good books and online tutorials to help you learn the rest of the language. Visit the official Python web page to find out more, \texttt{http://www.python.org}.
\end{document}

