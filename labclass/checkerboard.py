"""Robot demo: repainting a checkerboard."""
from robot import Robot
robby = Robot()
robby.load_board(2)  # load checkerboard pattern

for i in range(10):
    for i in range(10):
        colour = robby.look_down()
        if colour == 0:
            robby.paint(1)
        else:
            robby.paint(0)
        robby.move_forward()
    robby.turn_right()
    robby.turn_right()
    for i in range(10):
        robby.move_forward()
    robby.turn_left()
    robby.move_forward()
    robby.turn_left()

robby.wait()
robby.quit()
