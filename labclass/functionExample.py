"""Demo of using robot module."""
from robot import Robot

# define a function called drawLine


def draw_line():
    """Draw a line."""
    for j in range(10):
        robby.paint(2)
        robby.move_forward()

# Construct a robot and call it 'robby'

robby = Robot()

# Draw lines by calling the draw_line() function
draw_line()
robby.turn_right()
draw_line()

robby.wait()
robby.quit()
