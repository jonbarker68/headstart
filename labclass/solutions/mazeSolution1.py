"""Solution to the first maze problem."""
from robot import Robot

robby = Robot()

robby.load_board(3)  # path pattern

for i in range(100):
    robby.paint(2)
    colour = robby.look_ahead()
    if colour == 3:
        robby.move_forward()
        break
    elif colour != 1:
        robby.turn_right()
    else:
        robby.move_forward()

robby.wait()
