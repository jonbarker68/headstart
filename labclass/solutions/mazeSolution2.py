"""Solution to the second maze problem."""
from robot import Robot


def robot_ahead_or_right():
    """Move ahead if possible else turn right."""
    colour = robby.look_ahead()
    if colour > 0:
        robby.move_forward()
    else:
        robby.turn_right()

robby = Robot()

# load board 4
# ... will also work with board 3 and 5 but not with 6.
robby.load_board(4)

while 1:

    if robby.look_ahead() == 3:
        break
    robot_ahead_or_right()

    robby.turn_left()

    if robby.look_ahead() == 3:
        break
    robot_ahead_or_right()

robby.wait()
robby.quit()
