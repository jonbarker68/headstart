"""Solution to the rings problem."""
from robot import Robot


def draw_line(length, colour):
    """Draw a line of given length and colour."""
    for i in range(length):
        robby.paint(colour)
        robby.move_forward()


def draw_square(size, colour):
    """Draw a square of given size and colour."""
    for i in range(4):
        draw_line(size, colour)
        robby.turn_right()

robby = Robot()

for i in range(3):
    draw_square(5, i + 1)
    robby.move_forward()
    robby.move_forward()
    robby.turn_right()
    robby.move_forward()
    robby.move_forward()
    robby.turn_left()

robby.wait()
robby.quit()
